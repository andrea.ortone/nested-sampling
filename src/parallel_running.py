import numpy as np
import matplotlib.pyplot as plt
from subprocess import run
import threading, queue
import time

# diff_args:    1 - _N
#               2 - _nu
#               3 - _itMC
#               4 - _save_MC
#               5 - savefile


def worker():
    while True:
        item = q.get()
        print(f'\t****** {item} ******')
        run(item, shell=True)
        q.task_done()


N = [64]
for i in range(8):
    N.append(N[i]*2)
# N = [8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768]
nu = [0.1*i for i in range(1,10)]

q = queue.Queue()
#
# for n in N:
#     #q.put(f'./build/main {n} 300 f build/1.txt')  # this produces file *1.txt
#     q.put(f'./build/diffusive_main {n} 0.5 200 f build/diff1.txt')

# for i in range(6000):
#     # q.put(f'./build/main 1024 1 f build/2.txt')   # this produces file *2.txt  CLASSIC
#     q.put(f'./build/diffusive_main 6000 {1/np.e} 1 f build/diff2.txt') # da rifare con un numero tondo così listogramma viene piu bello

# q.put('./build/main 1024 6000 t build/3.txt')     # this produces file *lZlMC.txt and *3.txt

# cost = 722
# for n in nu:
#     q.put(f'./build/diffusive_main {int(-cost*np.log(n)/(1-n))} {n} 2000 f build/diff4.txt')

# for i in range(12):
#     threading.Thread(target=worker, daemon=True).start()
#     time.sleep(2)
#
# q.join()
