#include <iostream>
#include "nested.h"
#include <string>

using namespace std;


int main(int argc, char **argv) {
    pcg_extras::seed_seq_from<std::random_device>   seed_source;
    pcg32                                           gen(seed_source);
    uniform_real_distribution<double>               dis(0,1);

    if (argc>1) {
        // args:    1 - _N
        //          2 - _itMC
        //          3 - _save_MC
        //          4 - savefile
        bool    save_lZl=false;
        if (strcmp("t", argv[3])==0) save_lZl = true;
        NestedSampling test(50, 10000000, stoi(argv[1]), gen, dis, stoi(argv[2]), false, save_lZl);
        FILE *fp;

        test.run();

        fp = fopen(argv[4], "a");
        fprintf(fp, "%s\t%e\t%e\t%e\t%e\t%d\n", argv[1], test.logZ_lowerb, test.stdlo, test.logZ_upperb, test.stdu, test.iter());
        fclose(fp);

        cout << "done " << argv[1] << endl;
    }
    else {
        NestedSampling test(50, 10000000, 1000, gen, dis, 1, false, false);

        test.run();
        cout << "exact\t" << test.logZ_lowerb_exact << " < log Z_ex < " << test.logZ_upperb_exact << endl;
        cout << "MC   \t" << test.logZ_lowerb << " +- "<< test.stdlo << " < log Z < " << test.logZ_upperb << " +- "<< test.stdu << endl;
        cout << "\nLog_likelihood called " << test.L_counter << " times." << endl;
        cout << "\ndone" << endl;
    }

}


/*
MANCA:
    - terminazion
    - information H

NOTE: per confrontare con luca devi aggiungere -_dim*log(2*M_PI)/2 \sim 45.947 (normalizzazione della likelihood)
      credo basti cambiare log_likelihood e ogni volta che usi la relazione tra r <-> log_L
*/
