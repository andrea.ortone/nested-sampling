#include <iostream>
#include "diffusive_nested.h"
#include <string>

using namespace std;


int main(int argc, char **argv) {
    pcg_extras::seed_seq_from<std::random_device>   seed_source;
    pcg32                                           gen(seed_source);
    uniform_real_distribution<double>               dis(0,1);

    if (argc>1) {
        // args:    1 - _N
        //          2 - _nu
        //          3 - _itMC
        //          4 - _save_MC
        //          5 - savefile
        bool    save_lZl=false;
        if (strcmp("t", argv[4])==0) save_lZl = true;
        cout << int(-500./log(atof(argv[2]))) << "  " << stoi(argv[1]) << "  " <<  atof(argv[2]) << endl;
        DiffusiveNestedSampling test(50, int(-500./log(atof(argv[2]))), stoi(argv[1]), atof(argv[2]), gen, dis, stoi(argv[3]), false, save_lZl);

        FILE *fp;
        test.run();

        fp = fopen(argv[5], "a");
        fprintf(fp, "%s\t%s\t%e\t%e\t%d\n", argv[1],argv[2], (test.logZ_upperb_naive+test.logZ_lowerb_naive)/2, test.error, test.L_counter);
        fclose(fp);

        cout << "done" << endl;
    }
    else {
        // DiffusiveNestedSampling test(50,1000, 1000, 1./2.71828, gen, dis, 60, false, false);
        DiffusiveNestedSampling test(50,1024, 721, 0.5, gen, dis, 200, false, false);

        test.run();

        cout << test.logZ_lowerb_naive << " < log Z_naive < " << test.logZ_upperb_naive << "  ->  " << (test.logZ_upperb_naive+test.logZ_lowerb_naive)/2 <<" +- " << test.error << endl;
        // cout << "MC   \t" << test.logZ_lowerb << " +- "<< test.stdlo << " < log Z < " << test.logZ_upperb << " +- "<< test.stdu << endl;
        cout << "\nLog_likelihood called " << test.L_counter << " times." << endl;
        cout << "\ndone" << endl;
    }

}


/*
MANCA:
    - terminazion
    - information H

NOTE: per confrontare con luca devi aggiungere -_dim*log(2*M_PI)/2 \sim 45.947 (normalizzazione della likelihood)
      credo basti cambiare log_likelihood e ogni volta che usi la relazione tra r <-> log_L
*/
