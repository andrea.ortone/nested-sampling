import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.optimize import curve_fit

plt.style.use('style.yml')

def fit(x, A, B):
    return A*x+B

data1 = np.loadtxt('build/diffanalysis/diff1.txt', unpack=True)
data4 = np.loadtxt('build/diffanalysis/diff4.txt', unpack=True)
data4a = np.loadtxt('build/diffanalysis/diff4c.txt', unpack=True)

data4s = (data4 + data4a)/2

# # _N, _nu, lz, err

N = data1[0]

plt.errorbar(data1[0], data1[2], yerr=data1[3])
plt.plot([min(N), max(N)], [-115.001]*2)
plt.xscale('log')
plt.xlabel('N')
plt.ylabel(r'$\log{Z}$')
# plt.savefig('DNS_N.pdf', dpi=600)
plt.show()

plt.plot(data1[0], data1[3], marker='x', linestyle='')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N')
plt.ylabel(r'Error on $\log{Z}$')

opt, cov = curve_fit(
    fit, np.log(N), np.log(data1[3]))

X = np.linspace(min(N), max(N), 1000)
plt.plot(X, (X**opt[0])*np.e**opt[1], color='black', linestyle='--')
# plt.savefig('DNS_Nfit.pdf', dpi=600)
plt.show()
print(opt, np.sqrt(cov[0,0]), np.sqrt(cov[1,1]))

# -------------------------------------------------------------------- nu


plt.xlabel(r'$\nu$')
plt.ylabel(r'$\log{Z}$')
plt.errorbar(data4[1], data4[2], yerr=data4s[3])
plt.plot([min(data4[1]), max(data4[1])], [-115.001]*2)
# plt.savefig('nu.pdf', dpi=600)
plt.show()

plt.xlabel(r'$\nu$')
plt.ylabel(r'Error on $\log{Z}$')
plt.plot(data4s[1], data4s[3])
# plt.savefig('nuerror.pdf', dpi=600)
plt.show()




# -------------------------------------------------------------------- error


# diff2.txt are indipendent estimations of lZ(lowerb) (also L_i are extracted)
data = np.loadtxt('build/diffanalysis/difflZlMC.txt', unpack=True)

hs, _, _ = plt.hist(data, alpha=0.5, density=True, bins=20, color='C1')
plt.xlabel(r'$\log{Z}$')
plt.ylabel(r'p$\left(\log{Z}\right)$')
plt.plot([-115.001]*2, [0,1.05*max(hs)], color='black')
plt.savefig('DNS_MCLZ.pdf', dpi=600)
plt.show()

hs2, _, _ = plt.hist(data, alpha=0.5, label='MC', density=True, bins=20, color='C1')
mean_MC = data.mean()
std_MC = data.std()
print(f'MC \tmean_MC = {mean_MC} +- {std_MC}    {len(data)}')

data = np.loadtxt('build/diffanalysis/diff2.txt', unpack=True)

hs1, _, _ = plt.hist(data[0], alpha=0.5, label='Repetition', density=True, bins=20, color='C0')



plt.xlabel(r'$\log{Z}$')
plt.ylabel(r'p$\left(\log{Z}\right)$')
plt.legend()
plt.plot([-115.001]*2, [0,1.05*max((max(hs1), max(hs2)))], color='black')
plt.savefig('DNS_repVSmc.pdf', dpi=600)
plt.show()






# --------------------------------------------------------------------

# confronto DIFFUSIVE VS CLASSIC

data1_classic = np.loadtxt('build/analysis/1.txt', unpack=True)

plt.plot(data1_classic[5], data1_classic[2], label='classic')
plt.plot(data1[4], data1[3], label='diffusive')
plt.legend()
plt.xlabel(r'$\mathcal{L}(\theta) computations$')
plt.ylabel(r'Error on $\log{Z}$')
plt.xscale('log')
plt.yscale('log')
# plt.savefig('confronto.pdf', dpi=600)
plt.show()
