import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.optimize import curve_fit

# plt.style.use('style.yml')

def fit(x, A, B):
    return A*x+B

# N analysis

data = np.loadtxt('build/analysis/1.txt', unpack=True)

N = data[0]
plt.errorbar(N, data[1], data[2])
#plt.errorbar(N, data[3], data[4])
plt.plot([min(N), max(N)], [-115.001]*2)
plt.xscale('log')
plt.xlabel('N')
plt.ylabel(r'$\log{Z}$')
#plt.legend()
# plt.savefig('CNS_N.pdf', dpi=600)
plt.show()

plt.plot(N, data[4], marker='x', linestyle='')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('N')
plt.ylabel(r'Error on $\log{Z}$')

opt, cov = curve_fit(
    fit, np.log(N), np.log(data[4]))

X = np.linspace(min(N), max(N), 1000)
plt.plot(X, (X**opt[0])*np.e**opt[1], color='black', linestyle='--')
print(opt, np.sqrt(cov[0,0]), np.sqrt(cov[1,1]))

# plt.savefig('CNS_Nfit.pdf', dpi=600)
plt.show()


# ok, va come 1/sqrt(N)

# Error analysis

# 2.txt are indipendent estimations of lZ(lowerb) (also L_i are extracted)
data = np.loadtxt('build/analysis/2.txt', unpack=True)
N = data[0,0]
mean_l = data[1].mean()
mean_u = data[3].mean()
std_l = data[1].std()
std_u = data[3].std()

print(f'2\tmean_l = {mean_l} +- {std_l}  ({data[1].std()/np.sqrt(len(data[0]))})  {len(data[0])}')
print(f'2\tmean_u = {mean_u} +- {std_u}  ({data[3].std()/np.sqrt(len(data[0]))})  {len(data[0])}')


hs1, _, _ = plt.hist(data[1], alpha=0.5, label='repetition', density=True, bins=20)
#plt.hist(data[1], alpha=0.5, label='repetition', density=True)

lZl = np.loadtxt('build/analysis/lZlMC.txt')
# lZl = np.loadtxt('lZlMC.txt')

print(f'3\tmeanMC_l = {lZl.mean()} +- {lZl.std()}  ({lZl.std()/np.sqrt(len(lZl))})   ({len(lZl)})')

hs2, _, _ = plt.hist(lZl, alpha=0.5, label='MC', density=True, bins=20)
#plt.hist(lZl, alpha=0.5, label='MC', density=True)
plt.legend()
plt.xlabel(r'$\log{Z}$')
plt.ylabel(r'p$\left(\log{Z}\right)$')
plt.plot([-115.001]*2, [0,1.05*max((max(hs1), max(hs2)))], color='black')
# plt.savefig('CNS_repVSmc.pdf', dpi=600)
plt.show()



hs,_,_ = plt.hist(lZl, alpha=0.5, density=True, bins=20, color='C1')
plt.plot()
plt.xlabel(r'$\log{Z}$')
plt.ylabel(r'p$\left(\log{Z}\right)$')
plt.plot([-115.001]*2, [0,1.05*max(hs)], color='black')
# plt.savefig('CNS_MCLZ.pdf', dpi=600)
plt.show()
