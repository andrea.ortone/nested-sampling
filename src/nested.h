#ifndef NESTED_H
#define NESTED_H

#include <vector>
#include <utility>
#include <cmath>
#include <math.h>
#include <random>
#include <limits>
#include <float.h>
#include "pcg_cpp/include/pcg_random.hpp"

using namespace std;

/* some utilities */

double N_sphere_Vol(int N, double r);
double N_sphere_Area(int N, double r);
double mod(vector<double> & v);
double log_sum(double log_a, double log_b); //returns log(a+b)
double log_dif(double log_a, double log_b); //returns log(a-b), a must be > b

/* Nested Sampling class handler */

class NestedSampling {
private:
    bool _verbouse;
    bool _save_MC;
    int  _dim;
    int  _N;
    int  _iter;
    int  _itMC;
    vector<vector<double>>  _living_particles;
    vector<double>          _living_particles_log_L;
    vector<double>          _log_L;         // log likelihood of the samples
    vector<double>          _log_X;         // log mass function of the samples (exact calculation, only for this simple case)
    vector<double>          _log_pi;        // log prior of the samples
    pair<int, double>       _worst{0, 0.};  // int=index in _living_particles, double = logL                    //

    pcg32                                _gen;
    uniform_real_distribution<double>    _dis;

public:
    double  logZ_upperb_exact, logZ_lowerb_exact;
    double  logZ_upperb, logZ_lowerb, stdu, stdlo;
    double  H;
    int     L_counter;

    NestedSampling(int dim, int it, int N, pcg32 &g, uniform_real_distribution<double> &d, int itMC, bool verbouse=false, bool save_MC=false);
    double log_prior(vector<double> & th);          // R value to be changed
    double log_likelihood(vector<double> & th);
    vector<double> new_from_prior(double logL_min); // change it if the problem changes
    pair<int, double>  find_worst();
    void logZ_exact_calculator();
    void logZ_calculator();
    bool termination(int i);                             // change it if the problem changes
    void H_calculator();
    int iter();
    void run();


};

#endif
