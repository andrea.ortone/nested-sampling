#include <iostream>
#include "nested.h"

using namespace std;

double R = 45.;

/* some utilities */

double N_sphere_Vol(int N, double r) {
    return pow(M_PI,double(N)/2) * pow(r,N) / tgamma(double(N)/2+1);
}

double N_sphere_Area(int N, double r) {
    return N * pow(M_PI,double(N)/2) * pow(r,N-1) / tgamma(double(N)/2+1);
}

double mod(vector<double> & v) {
    double sum=0;
    for (auto d : v) sum += d*d;
    return sqrt(sum);
}

double log_sum(double log_a, double log_b) {
    if (log_a < log_b) return log_b + log(1 + exp(log_a - log_b));
    else return log_a + log(1 + exp(log_b - log_a));
}

double log_dif(double log_a, double log_b) {
    if (log_a < log_b) {
        cerr << "Error: log_dif with invalid input" << endl;
        exit(1);
    }
    else return log_a + log(1 - exp(log_b - log_a));
}


/* NestedSampling class methods */

NestedSampling::NestedSampling(int dim, int it, int N, pcg32 &g, uniform_real_distribution<double> &d, int itMC, bool verbouse, bool save_MC) {
    cout << "Initialization... (verbosity = " << verbouse << ")\n" << endl;
    _verbouse = verbouse;
    _save_MC = save_MC;
    _dim = dim;
    _iter = it;
    _itMC =itMC;
    _N = N;
    _living_particles=vector<vector<double>>(_N, vector<double>(_dim, 0.)); // !!!!!
    _living_particles_log_L.resize(_N);
    _gen = g;
    _dis = d;
    L_counter = 0;

    _log_L.resize(_iter);
    _log_X.resize(_iter);
    _log_pi.resize(_iter);

}

double NestedSampling::log_prior(vector<double> & th) {
    if (mod(th) > R) return -numeric_limits<double>::infinity();
    else return -log(N_sphere_Vol(_dim, R));
}

double NestedSampling::log_likelihood(vector<double> & th) {
    L_counter +=1;
    return -pow(mod(th),2)/2;
}

vector<double> NestedSampling::new_from_prior(double logL_min) {
    vector<double>      tmp=vector<double>(_dim, 0.), to_ret=vector<double>(_dim, 0.);
    // tmp = (r, theta, phi_1, ... , phi_n-2)   with r in [0,R], theta in [o,2pi], phi_i in [0,pi]

    if (logL_min<-DBL_MAX) tmp[0] = R*pow(_dis(_gen), 1./_dim);
    else tmp[0] = sqrt(-2*logL_min)*pow(_dis(_gen), 1./_dim);

    if (_verbouse) cout << "new r generated: " << tmp[0] << " (logL_min = " << logL_min << "<-> r_max =" << sqrt(-2*logL_min) << ")" << endl;

    tmp[1] = 2*M_PI*_dis(_gen);
    for(int i=2; i<_dim; i++) tmp[i] = acos(_dis(_gen));

    for(int i=0; i<_dim; i++) {
        to_ret[i] = tmp[0] * cos(tmp[i+2]);
        if (i<_dim-2) {
            for(int j=0; j<i; j++) {
                to_ret[i] *= sin(tmp[j+2]);
            }
        }
    }
    for(int j=0; j<_dim-2; j++) {
        to_ret[_dim-2] *= sin(tmp[j+2]);
        to_ret[_dim-1] *= sin(tmp[j+2]);
    }
    to_ret[_dim-2] *= cos(tmp[1]);
    to_ret[_dim-1] *= sin(tmp[1]);

    return to_ret;
}

pair<int, double>  NestedSampling::find_worst() {
    double  w=0.;
    int     index=0;
    for (int i=0; i<_N; i++){
        if (_living_particles_log_L[i] < w) {
            index=i;
            w=_living_particles_log_L[i];
        }
    }
    return make_pair(index, w);
}

void NestedSampling::logZ_exact_calculator() {
    double lZ = -numeric_limits<double>::infinity();
    double sum = -log(_N);      // useful to calculate log of L_mean of living particles
    for(int i=0; i<_iter-1; i++) {
        lZ = log_sum(lZ, _log_L[i] + log_dif(_log_X[i], _log_X[i+1]));
    }
    logZ_upperb_exact = lZ;
    lZ=-numeric_limits<double>::infinity();
    for(int i=1; i<_iter; i++) {
        lZ = log_sum(lZ, _log_L[i] + log_dif(_log_X[i-1], _log_X[i]));
    }
    logZ_lowerb_exact = lZ;

    for(int i=0; i<_N; i++) {
        sum = log_sum(sum, log_likelihood(_living_particles[i]));
    }
    sum += _log_X[_iter-1];
    logZ_upperb_exact = log_sum(logZ_upperb_exact, sum);
    logZ_lowerb_exact = log_sum(logZ_lowerb_exact, sum);
}

void NestedSampling::logZ_calculator() {
    double           lZu=0., lZl=0., lX1=0., lX2=0., sum=0., sum_b=-log(_N);
    FILE             *fp1, *fp2;
    fp1 = fopen("./testMC.txt", "w");
    if (_save_MC) fp2 = fopen("./lZlMC.txt", "w");


    for(int i=0; i<_N; i++) {
        sum_b = log_sum(sum, log_likelihood(_living_particles[i]));
    }
    stdu = stdlo = 0.;
    for(int j=0; j<_itMC; j++) {
        lZl=-numeric_limits<double>::infinity();
        lZu=-numeric_limits<double>::infinity();
        lX1 = 1.;
        lX2 = log(_dis(_gen))/double(_N);
        sum = sum_b;

        for(int i=0; i<_iter-1; i++) {
            if (j==0) fprintf(fp1, "%e\n", lX1);
            lX1 = lX2;
            lX2 = lX1+log(_dis(_gen))/double(_N);
            lZu = log_sum(lZu, _log_L[i] + log_dif(lX1, lX2));
            lZl = log_sum(lZl, _log_L[i+1] + log_dif(lX1, lX2));
        }
        if (j==0) fprintf(fp1, "%e\n", lX2);

        sum += lX2;
        lZu = log_sum(lZu, sum);
        lZl = log_sum(lZl, sum);

        if (_save_MC) fprintf(fp2, "%e\n", lZl);


        logZ_upperb += lZu;
        logZ_lowerb += lZl;
        stdu += pow(lZu,2);
        stdlo += pow(lZl,2);
        if (_verbouse) cout << "log_Z calculator: estimate " << j << lZl << " < " << lZu << endl;
    }
    logZ_upperb /= _itMC;
    logZ_lowerb /= _itMC;
    stdu = sqrt(stdu/_itMC - pow(logZ_upperb,2));
    stdlo = sqrt(stdlo/_itMC - pow(logZ_lowerb,2));

    // fclose(fp1);
    if (_save_MC) fclose(fp2);
}

bool NestedSampling::termination(int i) {
    if (_log_X[i] < -log(DBL_MAX) + 200) {
        cout << "Note: terminated before _iter was reached\n" << endl;
        return true;
    }
    else return false;
}

int NestedSampling::iter() {
    return _iter;
}


void NestedSampling::run() {
    // generate _N samples from prior

    for (int i=0; i<_N; i++) {
        _living_particles[i] = new_from_prior(-numeric_limits<double>::infinity());
        _living_particles_log_L[i] = log_likelihood(_living_particles[i]);
    }

    _worst = find_worst();

    for(int i=0; i<_iter; i++) {
        // cout << _worst.second << endl;

        _log_L[i] = _worst.second;
        _log_X[i] = log(N_sphere_Vol(_dim, sqrt(-2*_worst.second))/N_sphere_Vol(_dim,R));
        _log_pi[i] = log_prior(_living_particles[_worst.first]);

        _living_particles[_worst.first] = new_from_prior(_worst.second);
        _living_particles_log_L[_worst.first] = log_likelihood(_living_particles[_worst.first]);

        if (_verbouse) cout << "new_from_prior: updated worst log_L " << _worst.second << " to ";
        _worst = find_worst();
        if (_verbouse) cout << _worst.second << endl;

        if (termination(i)) _iter = i;
    }

    logZ_exact_calculator();
    logZ_calculator();

    FILE *fp;
    fp = fopen("./test.txt", "w");
    for(int j=0; j<_iter; j++) {
        fprintf(fp, "%e\t", _log_X[j]);
        fprintf(fp, "%e\n", _log_L[j]);
    }
    fclose(fp);
}
