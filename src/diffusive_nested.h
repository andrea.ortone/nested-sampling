#ifndef DIFFUSIVE_NESTED_H
#define DIFFUSIVE_NESTED_H

#include <vector>
#include <cmath>
#include <math.h>
#include <random>
#include <limits>
#include <float.h>
#include <algorithm>
#include "pcg_cpp/include/pcg_random.hpp"

using namespace std;

/* some utilities */

double N_sphere_Vol(int N, double r);
double N_sphere_Area(int N, double r);
double mod(vector<double> & v);
double log_sum(double log_a, double log_b); //returns log(a+b)
double log_dif(double log_a, double log_b); //returns log(a-b), a must be > b
double  p_logt(double y, int N, int q);

/* Nested Sampling class handler */

class DiffusiveNestedSampling {
private:
    bool    _verbouse;
    bool    _save_MC;
    int     _dim;
    int     _N_levels;
    int     _N;
    int     _itMC;
    double  _nu;
    double  _q, _barq;
    vector<double>          _log_L_levels;
    vector<double>          _log_L;      // log likelihood of the samples

    double                  _bm;
    pcg32                                _gen;
    uniform_real_distribution<double>    _dis;

public:
    int     L_counter;
    double  logZ_lowerb_naive, logZ_upperb_naive, error;

    long double                  M, V;

    DiffusiveNestedSampling(int dim, int N_levels, int N, double nu, pcg32 &g, uniform_real_distribution<double> &d, int itMC, bool verbouse=false, bool save_MC=false);
    double log_prior(vector<double> & th);          // R value to be changed
    double log_likelihood(vector<double> & th);
    vector<double> new_from_prior(double logL_min); // change it if the problem changes
    double find_quartile(vector<double> & t);         // also reversal order tmp
    void logZ_naive_calculator();
    void findMandV();
    double gauss_generator();
    void error_calculator();
    void run();

};

#endif
