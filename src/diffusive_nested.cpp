#include <iostream>
#include "diffusive_nested.h"

using namespace std;

double R = 45.;
long double Multiplier=1.;

/* some utilities */

double N_sphere_Vol(int N, double r) {
    return pow(M_PI,double(N)/2) * pow(r,N) / tgamma(double(N)/2+1);
}

double N_sphere_Area(int N, double r) {
    return N * pow(M_PI,double(N)/2) * pow(r,N-1) / tgamma(double(N)/2+1);
}

double mod(vector<double> & v) {
    double sum=0;
    for (auto d : v) sum += d*d;
    return sqrt(sum);
}

double log_sum(double log_a, double log_b) {
    if (log_a < log_b) return log_b + log(1 + exp(log_a - log_b));
    else return log_a + log(1 + exp(log_b - log_a));
}

double log_dif(double log_a, double log_b) {
    if (log_a < log_b) {
        cerr << "Error: log_dif with invalid input" << endl;
        exit(1);
    }
    else return log_a + log(1 - exp(log_b - log_a));
}

double  p_logt(double y, int N, int q) {          // approximation _q always int(_N*_nu)
    long double to_ret = 1;
    for (int i=0; i<q+1; i++) to_ret *= Multiplier*exp(y);
    for (int i=0; i< N-q-1; i++) to_ret *= Multiplier*(1-exp(y));
    //printf( "%f\t%d\t %e\t %e \t%e\n", y, (N-q), exp((q+1)*y), pow((1-exp(y)), N-q-1) , (N-q)*exp((q+1)*y)*pow((1-exp(y)), N-q-1));
    return to_ret*(N-q);
}


/* DiffusiveNestedSampling class methods */

DiffusiveNestedSampling::DiffusiveNestedSampling(int dim, int N_levels, int N, double nu, pcg32 &g, uniform_real_distribution<double> &d, int itMC, bool verbouse, bool save_MC) {
    cout << "Initialization... (verbosity = " << verbouse << ")\n" << endl;
    _verbouse = verbouse;
    _save_MC = save_MC;
    _dim = dim;
    _N_levels = N_levels;
    _N = N;
    _itMC = itMC;
    _nu = nu;
    _gen = g;
    _dis = d;
    L_counter = 0;

    _q = int(_N*_nu);
    _barq = _N - _q;

    _log_L.resize(_barq*_N_levels+_N);
    _log_L_levels.resize(_N_levels);

    _bm = numeric_limits<double>::infinity();
    findMandV();
    cout << "M = " << M << "  V = " << sqrt(V) << endl;
    cout << "_N = " << _N << "  _N_levels = " <<_N_levels <<"  _nu = " << _nu << "  _q = " << _q << "  logL_size = " << _barq*_N_levels+_N << endl;
}

double DiffusiveNestedSampling::log_prior(vector<double> & th) {
    if (mod(th) > R) return -numeric_limits<double>::infinity();
    else return -log(N_sphere_Vol(_dim, R));
}

double DiffusiveNestedSampling::log_likelihood(vector<double> & th) {
    L_counter +=1;
    return -pow(mod(th),2)/2;
}

vector<double> DiffusiveNestedSampling::new_from_prior(double logL_min) {
    vector<double>      tmp=vector<double>(_dim, 0.), to_ret=vector<double>(_dim, 0.);
    // tmp = (r, theta, phi_1, ... , phi_n-2)   with r in [0,R], theta in [o,2pi], phi_i in [0,pi]

    if (logL_min<-DBL_MAX) tmp[0] = R*pow(_dis(_gen), 1./_dim);
    else tmp[0] = sqrt(-2*logL_min)*pow(_dis(_gen), 1./_dim);

    if (_verbouse) cout << "new r generated: " << tmp[0] << " (logL_min = " << logL_min << "<-> r_max =" << sqrt(-2*logL_min) << ")" << endl;

    tmp[1] = 2*M_PI*_dis(_gen);
    for(int i=2; i<_dim; i++) tmp[i] = acos(_dis(_gen));

    for(int i=0; i<_dim; i++) {
        to_ret[i] = tmp[0] * cos(tmp[i+2]);
        if (i<_dim-2) {
            for(int j=0; j<i; j++) {
                to_ret[i] *= sin(tmp[j+2]);
            }
        }
    }
    for(int j=0; j<_dim-2; j++) {
        to_ret[_dim-2] *= sin(tmp[j+2]);
        to_ret[_dim-1] *= sin(tmp[j+2]);
    }
    to_ret[_dim-2] *= cos(tmp[1]);
    to_ret[_dim-1] *= sin(tmp[1]);

    return to_ret;
}

double DiffusiveNestedSampling::find_quartile(vector<double> & t) {
    double  r=_N*_nu-_q;
    sort(t.begin(), t.end(), greater<double>());

    if (_dis(_gen)<r) return t[_q+1];
    else return t[_q];
}


void DiffusiveNestedSampling::findMandV(){
    double      epsilon=0.0001, min=-8, t=0-epsilon;
    long double N=0.;
    M=V=0.;
    FILE *fp;
    fp = fopen("plog.txt", "w");
    while (isnan(V) || Multiplier<1.1) {
        M=V=N=0;
        t=0-epsilon;
        while (t>min) {
            N += (p_logt(t-epsilon, _N, _q) + p_logt(t, _N, _q))*epsilon/2;
            M += t*(p_logt(t-epsilon, _N, _q) + p_logt(t, _N, _q))*epsilon/2;
            V += t*t*(p_logt(t-epsilon, _N, _q) + p_logt(t, _N, _q))*epsilon/2;
            fprintf(fp, "%e\t%e\n", t, p_logt(t, _N, _q));
            //printf("\t %Le \t %Le\n", M,V);
            t-=epsilon;
        }
        M /= N;
        V /= N;
        Multiplier *= 2;
        cout << "\t\t Multiplier = " << Multiplier << endl;
    }
    fclose(fp);
    V = V-pow(M,2);
}

double DiffusiveNestedSampling::gauss_generator() {
    if (_bm > DBL_MAX) {
        double u1 = _dis(_gen), u2 = _dis(_gen);
        _bm = sqrt(-2*log(u1))*cos(2*M_PI*u2)*sqrt(V)+M;
        return sqrt(-2*log(u1))*sin(2*M_PI*u2)*sqrt(V)+M;
    }
    else {
        double tmp = _bm;
        _bm = numeric_limits<double>::infinity();
        return tmp;
    }
}

void DiffusiveNestedSampling::logZ_naive_calculator() {
    double          lx1, lx2, lZ = -numeric_limits<double>::infinity();
    vector<double>  Xlev(_N_levels,0);
    FILE *fp;
    fp = fopen("difftest2.txt", "w");        // all

    Xlev[0] = 0;
    for(int i=1; i<_N_levels; i++) {
        Xlev[i] = Xlev[i-1] + log(_nu);
    }

    lx1 = 0.;
    lx2 = log_dif(Xlev[0], log_dif(Xlev[0], Xlev[1]) - log(_barq));

    for(int i=0; i<_N_levels-1; i++) {
        for(int j=1; j<_barq; j++) {
            // cout << lx1<< "\t" << lx2<< "\t" <<Xlev[i] <<"  "<< Xlev[i+1]<<"\t" << _log_L[i*_barq+j]<<"\t" << lZ << "\t||\t" << log_dif(Xlev[i], Xlev[i+1])- log(_barq) << endl;
            lZ = log_sum(lZ, _log_L[i*_barq+j] + log_dif(lx1, lx2));
            lx1 = lx2;
            lx2 = log_dif(Xlev[i], log_dif(Xlev[i], Xlev[i+1])-log(_barq)+log(j));
            fprintf(fp, "%e\t%e\n", lx2, _log_L[i*_barq+j]);
        }
    }
    logZ_upperb_naive = lZ;
    fclose(fp);

    lx1 = 0.;
    lx2 = log_dif(Xlev[0], log_dif(Xlev[0], Xlev[1]) - log(_barq));
    lZ=-numeric_limits<double>::infinity();
    for(int i=0; i<_N_levels-1; i++) {
        for(int j=1; j<_barq-1; j++) {
            // cout << lx1<< "\t" << lx2<< "\t" <<Xlev[i] <<"  "<< Xlev[i+1]<<"\t" << _log_L[i*_barq+j]<<"\t" << lZ << "\t||\t" << log_dif(Xlev[i], Xlev[i+1])- log(_barq) << endl;
            lZ = log_sum(lZ, _log_L[i*_barq+j+1] + log_dif(lx1, lx2));
            lx1 = lx2;
            lx2 = log_dif(Xlev[i], log_dif(Xlev[i], Xlev[i+1])-log(_barq)+log(j));
        }
    }
    logZ_lowerb_naive = lZ;
}

void DiffusiveNestedSampling::error_calculator() {
    double          sum=0.,std=0., lx1, lx2, lZ;
    vector<double>  Xlev(_N_levels,0);
    FILE *fp, *fp2;
    fp = fopen("diffMC.txt", "w");        // all
    if (_save_MC) fp2=fopen("difflZlMC.txt", "w");
    for(int k=0; k<_itMC; k++) {
        lZ=-numeric_limits<double>::infinity();
        Xlev[0] = 0;
        for(int i=1; i<_N_levels; i++) {
            Xlev[i] = Xlev[i-1] + gauss_generator();
        }

        lx1 = 0.;
        lx2 = log_dif(Xlev[0], log_dif(Xlev[0], Xlev[1]) - log(_barq));

        for(int i=0; i<_N_levels-1; i++) {
            for(int j=1; j<_barq; j++) {
                // cout << lx1<< "\t" << lx2<< "\t" <<Xlev[i] <<"  "<< Xlev[i+1]<<"\t" << _log_L[i*_barq+j]<<"\t" << lZ << "\t||\t" << log_dif(Xlev[i], Xlev[i+1])- log(_barq) << endl;
                lZ = log_sum(lZ, _log_L[i*_barq+j] + log_dif(lx1, lx2));
                lx1 = lx2;
                lx2 = log_dif(Xlev[i], log_dif(Xlev[i], Xlev[i+1])-log(_barq)+log(j));
                if (k==0) fprintf(fp, "%e\t%e\n", lx2, _log_L[i*_barq+j]);
            }
        }
        if (_save_MC) fprintf(fp2, "%e\n", lZ);
        sum += lZ;
        std += lZ*lZ;
    }
    fclose(fp);
    if (_save_MC) fclose(fp2);
    error = sqrt( std/_itMC - pow(sum/_itMC,2) );
}

void DiffusiveNestedSampling::run() {
    vector<double>  tmp(_N,0.), t(_dim,0);
    double          lx=0;
    int             i=0, j=0;

    FILE *fp;
    fp = fopen("difftest.txt", "w");        // levels!

    cout << _log_L.size() << "\t" << tmp.size() <<"\t" << _q << endl;

    // generate _N_levels
    _log_L_levels[0] = -numeric_limits<double>::infinity();
    for(j=0; j<_q; j++) {
        t = new_from_prior(-numeric_limits<double>::infinity());
        _log_L[j] = tmp[j] = log_likelihood(t);
    }

    for(i=0; i<_N_levels-1;i++) {
        for(j=0; j<_barq; j++) {
            t = new_from_prior(_log_L_levels[i]);
            _log_L[_q+i*_barq+j] = tmp[_q+j] = log_likelihood(t);
        }
        _log_L_levels[i+1] = find_quartile(tmp);
        lx += log(_nu);
        fprintf(fp, "%e\t%e\n", lx, _log_L_levels[i+1]);

        if (lx < -log(DBL_MAX) + 200) {                                         // TERMINAZIONE
            cout << "Note: terminated before n_levels was reached\n" << endl;
            i+=1;
            break;
        }
    }
    _log_L.resize(_q+(i-1)*_barq+j);
    fclose(fp);

    sort(_log_L.begin(), _log_L.end());

    logZ_naive_calculator();

    error_calculator();
}
